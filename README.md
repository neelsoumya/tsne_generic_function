# tsne_generic_function

Generic function for tSNE visualization



INSTALLATION:

        pip3 install -U scikit-learn

Usage:

        python3 tsne_generic.py

Adapted from:

        https://scikit-learn.org/stable/auto_examples/manifold/plot_t_sne_perplexity.html
