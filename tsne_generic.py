#####################################################
# tSNE visualization
#
# INSTALLATION:
#   pip3 install -U scikit-learn
#
# Usage:
#   python3 tsne_generic.py
#
# Adapted from:
#   https://scikit-learn.org/stable/auto_examples/manifold/plot_t_sne_perplexity.html
#
#####################################################


###################################################
# Load libraries
###################################################
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from sklearn import manifold, datasets


def function_tsne_generic(f_matrix,
                          perplexities):

    """
    Function for tSNE
    Returns:

    Code adapted from:
    https://scikit-learn.org/stable/auto_examples/manifold/plot_t_sne_perplexity.html

    """

    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.ticker import NullFormatter
    from sklearn import manifold, datasets

    n_components = 2
    (fig, subplots) = plt.subplots(3, 5, figsize=(15, 8))

    X = f_matrix
    # pdb.set_trace()

    for i, perplexity in enumerate(perplexities):
        ax = subplots[1][i + 1]

        # t0 = time()
        tsne = manifold.TSNE(n_components=n_components, init='random',
                             random_state=0, perplexity=perplexity)
        Y = tsne.fit_transform(X)
        # t1 = time()
        # print("S-curve, perplexity=%d in %.2g sec" % (perplexity, t1 - t0))

        ax.set_title("Perplexity=%d" % perplexity)
        ax.scatter(Y[:, 0], Y[:, 1],  cmap=plt.cm.viridis) # c=color,
        ax.xaxis.set_major_formatter(NullFormatter())
        ax.yaxis.set_major_formatter(NullFormatter())
        ax.axis('tight')


    # plt.show()
    plt.savefig('tsne_visualization.png')

    # pdb.set_trace()



###################################################
# Load data
###################################################
n_samples = 300
X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

###################################################
# Call generic tSNE function
###################################################
perplexities = [5, 30, 50, 100]
function_tsne_generic(f_matrix=X, perplexities=perplexities)

